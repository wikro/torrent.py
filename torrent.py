#!/usr/bin/env python

import sys
import time
from os import walk
from os.path import join, split, basename, isfile, isdir, getsize
from hashlib import sha1

# List of known open trackers that can be used
openTrackers = [
	"udp://tracker.openbittorrent.com:80",
	"udp://tracker.publicbt.com:80",
	"udp://tracker.istole.it:80",
	"udp://tracker.btzoo.eu:80/announce",
	"udp://open.demonii.com:1337/announce",
	"http://opensharing.org:2710/announce",
	"http://announce.torrentsmd.com:8080/announce.php",
	"http://announce.torrentsmd.com:6969/announce",
	"http://bt.careland.com.cn:6969/announce",
	"http://i.bandito.org/announce",
	"http://bttrack.9you.com/announce",
	"udp://tracker.coppersurfer.tk:6969",
	"udp://tracker.leechers-paradise.org:6969"
]

# Used for parsing arguments passed to the script
from argparse import ArgumentParser
argparser = ArgumentParser(description="Create a torrent file used for sharing a file or directory using the bittorrent protocol.")
argparser.add_argument("filePath", help="File or diretory to create a torrent file for.", metavar="file")
argparser.add_argument("-o", dest="outputFile", nargs="?", required=False, help="Specify a name for the torrent file created instead of the default.")
argparser.add_argument("-k", dest="chunkSize", nargs="?", type=int, required=False, help="Chunk size when creating the torrent in multiples of 8 in kilobytes, e.g. 512.", default=2**9)
argparser.add_argument("-c", dest="comment", nargs="?", required=False, help="Add a comment to the torrent file.", default="Created using torrent.py")
argparser.add_argument("-u", dest="createdBy", nargs="?", required=False, help="Name the creator of the torrent file.", default="torrent.py")
argparser.add_argument("-t", dest="tracker", nargs="+", required=False, help="Tracker(s) to use for the torrent file. Default is a list of known open trackers.", default=openTrackers)
argparser.add_argument("--notracker", dest="noTracker", required=False, help="Create torrent without tracker(s).", action="store_true")

def Bencode(obj):
	if isinstance(obj, int):
		return "i" + str(obj) + "e" # 567 turns into i567e
	elif isinstance(obj, str):
		return str(len(obj)) + ":" + obj # "hello" turns into 5:hello
	elif isinstance(obj, list):
		items = [Bencode(item) for item in obj] # Encode every item if it's a list object, and create a new list of encoded items
		return "l" + "".join(items) + "e" # Join all encoded items in the list into a string and surround with l and e: l7:encoded4:list2:of7:stringse
	elif isinstance(obj, dict):
		items = [Bencode(key) + Bencode(obj[key]) for key in sorted(obj.keys())] # Same as with lists but in key-value pairs, {"name": "debian"} = 4:name6:debian
		return "d" + "".join(items) + "e" # Same with lists, join items into a string and surround with d and e.
	raise ValueError("Allowed types: int, str, list, dict; not %s", type(obj))

def GeneratePieces(fileList, pieceLength):
	"""
	Read a list of files in chunks of pieceLength (except maybe the last chunk),
	generate a sha1 hash of the chuck and yield it
	"""
	remaining = "" # The remaining data from the last file processed becase it was less than pieceLength
	for item in fileList:
		with open(item, "rb") as f:
			while 1:
				data = remaining
				data += f.read(pieceLength - len(remaining)) # Since any remaining data is getting prefixed, we want to read less from the next file to not go over pieceLength
				remaining = ""
				if not data:
					break
				if len(data) < pieceLength: # If the remaining data was less than pieceLength, store it for appending to the data in the next file
					remaining = data
					break
				yield sha1(data).digest()
	if len(remaining) > 0:
		yield sha1(remaining).digest() # Hash the last chuck that may be less than pieceLength

def TruncateAndSplitPath(filePath, rootPath):
	"""
	Turns this: /path/to/root/file.iso
	Into this: ["file.iso"]

	Turns this: /path/to/root/subdir/file2.iso
	Into this: ["subdir", "file.iso"]
	"""
	path = []
	filePath = filePath[len(rootPath):]
	while 1:
		filePath, tail = split(filePath) # During the first loop, tail should be the file name
		if tail != "":
			path.append(tail)
		else:
			break

	path.reverse() # Since we started with file and the the directory it in, the list is reversed, so we reverse to get it in the correct order
	return path

def CreateFileList(dirPath):
	"""
	Goes through all files and folders and creates a list of dicts
	containing file path and length information in dirPath

	[{ "path": "/path/to/file.iso", "length": 3465464 }, { "path": "/path/to/readme.txt", "length": 768 }, ...]
	"""
	fileList = []
	for root, dirs, files in walk(dirPath):
		for fileName in files:
			filePath = join(root, fileName)
			length = getsize(filePath)

			fileInfo = {
				"path": filePath,
				"length": length
			}
			fileList.append(fileInfo.copy())

	return fileList

def CreateTorrent(path = None, noTracker = False, announceList = [], pieceLength = 2**9, output = None, comment = "Created using torrent.py", createdBy = "torrent.py"):
	"""
	Put together the dictionary necessary to create a torrent file using parameters.

	Single-file example:
	{
		"announce": "http://tracker.site1.com/announce",
		"announce-list": [["http://tracker.site1.com/announce", "http://tracker.site2.com/announce"]],
		"comment": "A comment",
		"created by:" "Me",
		"creation date:": 1466450900,
		"encoding": "utf-8",
		"info":
		{
			"name": "debian.iso",
			"piece length": 262144,
			"length": 678301696,
			"pieces": "841ae846bc5b6d7bd6e9aa3dd9e551559c82abc1...d14f1631d776008f83772ee170c42411618190a4"
		}
	}

	Multi-file example:
	{
		"announce": "http://tracker.site1.com/announce",
		"announce-list": [["http://tracker.site1.com/announce", "http://tracker.site2.com/announce"]],
		"comment": "A comment",
		"created by:" "Me",
		"creation date:": 1466450900,
		"encoding": "utf-8",
		"info":
		{
			"name": "directoryName",
			"piece length": 262144,
			"files":
			[
				{"path": ["111.txt"], "length": 111},
				{"path": ["222.txt"], "length": 222}
			],
			"pieces": "6a8af7eda90ba9f851831073c48ea6b7b7e9feeb...8a43d9d965a47f75488d3fb47d2c586337a20b9f"
		}
	}

	"""
	pieceLength = pieceLength * 1024 # Multiply by 1024 because we assume the pieceLength is in kilobytes, and pieceLength needs to be in bytes
	path = path.rstrip("\\/") # Remove trailing \ or /

	info = {
		"name": basename(path),
		"piece length": pieceLength
	}

	if isdir(path):
		fileList = CreateFileList(path)
		files = []
		for index, fileItem in enumerate(fileList):
			files.append(fileItem["path"])
			fileList[index]["path"] = TruncateAndSplitPath(fileItem["path"], path)
		info["files"] = fileList
		info["pieces"] = "".join(GeneratePieces(files, pieceLength))
	elif isfile(path):
		info["length"] = getsize(path)
		info["pieces"] = "".join(GeneratePieces([path], pieceLength))

	torrent = {
		"comment": comment,
		"created by": createdBy,
		"creation date": int(time.time()), # Current Unix time stamp
		"encoding": "utf-8",
		"info": info
	}

	if len(announceList) > 0 and noTracker == False:
		torrent["announce"] = announceList[0]

		if len(announceList) > 1:
			torrent["announce-list"] = [announceList] # torrent-file specification requires the list to be in a 1 length list
	else:
		torrent["announce"] = "" # announce still has to be specified even if it's empty

	torrent = Bencode(torrent) # Bencode the torrent dict

	torrentFile = output if output != None else "%s.torrent" % info["name"]
	with open(torrentFile, "wb") as f:
		f.write(torrent)

if __name__ == "__main__":
	args = argparser.parse_args()
	CreateTorrent(args.filePath, args.noTracker, args.tracker, args.chunkSize, args.outputFile, args.comment, args.createdBy)
