usage: torrent.py [-h] [-o [OUTPUTFILE]] [-k [CHUNKSIZE]] [-c [COMMENT]]
                  [-u [CREATEDBY]] [-t TRACKER [TRACKER ...]] [--notracker]
                  file

Create a torrent file used for sharing a file or directory using the
bittorrent protocol.

positional arguments:
  file                  File or diretory to create a torrent file for.

optional arguments:
  -h, --help            show this help message and exit
  -o [OUTPUTFILE]       Specify a name for the torrent file created instead of
                        the default.
  -k [CHUNKSIZE]        Chunk size when creating the torrent in multiples of 8
                        in kilobytes, e.g. 512.
  -c [COMMENT]          Add a comment to the torrent file.
  -u [CREATEDBY]        Name the creator of the torrent file.
  -t TRACKER [TRACKER ...]
                        Tracker(s) to use for the torrent file. Default is a
                        list of known open trackers.
  --notracker           Create torrent without tracker(s).
